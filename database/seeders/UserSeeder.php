<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = [
            [
                'name'=>'aladesiun temitope',
                'email'=>'aladesiuntope@gmail.com',
                'password'=>'Admin@12345',
            ],
            [
            'name'=>'aladesiun pelumi',
            'email'=>'aladesiunpelumi@gmail.com',
            'password'=>'Admin@12345',
        ]
        ];
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
